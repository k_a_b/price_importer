require_relative 'lib/db_uploader'

files ||= begin
  Dir.chdir('./tmp')

  Dir['*.csv']
end

DbUploader.call(files)

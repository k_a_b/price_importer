# frozen_string_literal: true

require 'csv'
require 'pry'

class CsvParser
  COLUMN_NAMES_MAP = {
    brand: %w[Производитель],
    code: %w[Артикул Номер],
    stock: %w[Количество Кол-во],
    cost: %w[Цена],
    name: %w[Наименование НаименованиеТовара]
  }.freeze

  def self.call(file_name)
    new.prepare(file_name)
  end

  def prepare(file_name)
    parts = []

    CSV.foreach(file_name, headers: true) do |row|
      brand, code, stock, cost, name = row_values(row)

      next unless brand && code && stock && cost

      parts << {
        brand: brand,
        code: code,
        stock: stock,
        cost: cost,
        name: name
      }
    end

    parts.compact
  end

  def row_values(row)
    [
      value(:brand, row)&.capitalize,
      value(:code, row)&.capitalize,
      value(:stock, row)&.gsub(/\D/, '')&.to_i,
      value(:cost, row)&.to_f,
      value(:name, row)
    ]
  end

  def value(field, row)
    COLUMN_NAMES_MAP[field].map { |name| row[name] }.join
  end
end

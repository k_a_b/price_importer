# frozen_string_literal: true

require_relative 'csv_parser'
require 'active_record'
require 'pg'

ActiveRecord::Base.establish_connection(
  adapter: 'postgresql',
  encoding: 'unicode',
  database: 'test',
  username: 'postgres',
  password: ''
)

class DbUploader
  def self.call(files)
    new.upload(files)
  end

  def upload(files)
    files.each do |file|
      price = CsvParser.call(file)

      # not work without including in app
      ActiveRecord::Base.transaction do
        Product.delete_all

        Product.upsert_all(price)
      end
    end
  end
end

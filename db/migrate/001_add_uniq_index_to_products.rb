# frozen_string_literal: true

class AddUniqIndexToProducts < ActiveRecord::Migration[5.1]
  def change
    add_index :products,
              %i[code brand],
              name: 'index_product_on_code_brand',
              unique: true
  end
end

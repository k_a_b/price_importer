# frozen_string_literal: true

require_relative '../lib/csv_parser'

RSpec.describe CsvParser do
  subject { described_class.call(file_name) }

  let(:file_name) { './spec/fixtures/price_1.csv' }

  let(:expected_keys) do
    %i[brand code stock cost name]
  end

  it 'prepare price' do
    res = subject

    expect(res.count).to eq 14
    expect(res.sample.keys).to eq expected_keys
  end
end
